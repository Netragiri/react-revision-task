import React from 'react'
import image from "../assets/Images/image.png"

function Home() {
  return (
    <div className='container'>
      <div className='row'>
        <div className='col-md-4 mt-5'>
          <h1>Welcome to home </h1>
        </div>
        <div className='col-md-8'>
          <img src={image}></img>
        </div>
      </div>

    </div>
  )
}

export default Home
