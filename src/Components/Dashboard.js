import React, { useCallback, useEffect, useMemo, useRef, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { useNavigate } from 'react-router-dom'
import { detaildata } from '../Config/Comman'
import { fetchdata } from '../Redux/News/Actions'
import { getUsers } from '../Redux/User/UserActions'
import { detailhandler } from '../Utills/CommanFunc'
import { LogInAuth } from '../Utills/LogInAuth'
import Pagination from './Pagination'

function Dashboard() {
    const data = useSelector((state) => state.newslist)
    // const users = useSelector((state) => state.userlist)
    const [search, setSearch] = useState("")
    const [searchresult, setSearchresult] = useState([])
    const [filteredOrders, setfilteredOrders] = useState(data?.data?.articles)
    const [currentpage, setCurrentPage] = useState(1)
    const [newsperpage, setNewsperPage] = useState(5)
    const [sort, setSort] = useState("")


    const auth = LogInAuth()
    const newref = useRef();
    const [count, setCount] = useState(0)
    const dispatch = useDispatch()
    const nav = useNavigate()
    

    useEffect(() => {
        dispatch(fetchdata())
        // dispatch(getUsers())
        
        // console.log(users)
        // console.log(data)
    }, [])
    useEffect(()=>{
        setfilteredOrders(data.data.articles)
        // console.log(data.data.articles)
    },[data])

    const numberoftimes = useMemo(() => {
        return `Welcome ${auth.user}`;
    }, [auth])


    const counter = () => {
        return setCount(3)
    }

    const searchhandler = (e) => {
        setSearch(e.target.value)
        // console.log(data?.data?.articles)
        if (e.target.value.length === 3) {
            setSearchresult(data?.data?.articles?.filter((i) =>
                // console.log(i)
                i?.author?.toLowerCase().includes(e.target.value.toLowerCase())
            ))
        }
    }

    const indexOfLastpage = currentpage * newsperpage;
    const indexOffirstpage = indexOfLastpage - newsperpage;
    const currentNews = filteredOrders?.slice(indexOffirstpage, indexOfLastpage)

    const paginate = (pagenumber, e) => {
        e.preventDefault()
        setCurrentPage(pagenumber)
    }

    useEffect(() => {
        filterhandler()
    }, [sort])
    const filterhandler = () => {
        let array = data?.data?.articles
        if (sort == "atoz") {
            array.sort(function (a, b) {
                a = a?.name?.toLowerCase();
                b = b?.name?.toLowerCase();
                return ((a == b) ? 0 : ((a > b) ? 1 : -1));
            })
        }
        else if (sort == "ztoa") {
            array.sort(function (a, b) {
                a = a?.name?.toLowerCase();
                b = b?.name?.toLowerCase();
                return ((a == b) ? 0 : ((a < b) ? 1 : -1));
            })
        }
        else {
            setfilteredOrders(array)
        }
        setfilteredOrders(array)
    }
    return (
        <div className='container'>
            <div className='row'>
                <div class="dropdown text-start mt-2 col-md-10">
                    <input type="text" aria-expanded="false" value={search} onChange={(e) => searchhandler(e)} className={search.length == 3 ? "dropdown-toggle-show form-control m-2" : "dropdown-toggle form-control m-2"} placeholder='search for news'></input>
                    <ul className={search?.length == 3 ? "dropdown-menu show" : "dropdown-menu"} >
                        {searchresult?.length !== 0 ? searchresult?.map((item) => (
                            <li><button class="dropdown-item form-control" href="#" onClick={()=>detailhandler(item,nav)}>{item.author}</button></li>

                        )) : <li>not found</li>}

                    </ul>
                </div>
                <div class="col-md-2 dropdown mt-4">
                    <select  class="form-select form-select-sm mb-2" aria-label=".form-select-lg example" onChange={(e) => setSort(e.target.value)}>
                        <option value="nofilter" defaultValue="selected">No filter</option>
                        <option value="atoz">A to Z</option>
                        <option value="ztoa">Z to A</option>
                    </select>
                </div>

            </div>
            <div className='row'>
      {currentNews?.map((item)=>(
          <div class="card border-secondaryk mb-3 mt-4 col-md-5" style={{maxWidth: "600px"}}>
          <div class="row g-0">
            <div class="col-md-4 mt-4">
              <img src={item.urlToImage} class="img-fluid rounded-start" alt="..." style={{height:"150px"}} />
            </div>
            <div class="col-md-8">
              <div class="card-body"style={{height: "200px"}} >
                <h5 class="card-title fw-bold">{item.name}</h5>
                <p class="card-text fst-italic">{item.title.slice(0,60).concat("...")}</p>
                <button className='btn btn-secondary btn-sm mt-3' onClick={()=>detailhandler(item,nav)} >more details</button>
                                            <button className='btn btn-outline-secondary btn-sm ms-3 mt-3' onClick={() => {
                                                nav(`/update_news/${item.id}`, {
                                                    state: { array: item }
                                                })
                                            }}>update news</button>
              </div>
                <p class="card-text mb-2"><small class="text-muted">{item.publishedAt}</small></p>
            </div>
          </div>
        </div>
      ))}
      <Pagination newsperpage={newsperpage} totalnews={filteredOrders?.length} paginate={paginate} /> 
      </div>
   

        </div>
    )
}

export default Dashboard
 