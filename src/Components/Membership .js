import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { useLinkClickHandler, useNavigate } from 'react-router-dom'
import { buyplan, getmemberships } from '../Redux/membership/membershipAction'
import UserPlanDetails from './User/UserPlanDetails'

function Membership() {
    const data = useSelector((state) => state.membershiplist)
    const user=JSON.parse(localStorage.getItem('user'))
    const dispatch = useDispatch()
    const navigate = useNavigate()


    useEffect(() => {
        dispatch(getmemberships())
        // console.log(data?.data?.memberships)
    }, [])
    

    function getRandomColor() {
        var letters = '0123456789ABCDEF'.split('');
        var color = '#';
        for (var i = 0; i < 6; i++) {
            color += letters[Math.floor(Math.random() * 16)];
        }
        return color;
    }
    return (
        <div className='container p-5'>
            <div className='row'>
                {user.current_plan !== null ? <UserPlanDetails />:
                
                data?.data?.memberships?.map((item) => {
                    // console.log(item)
                    return <div class="card  m-3  " style={{ maxWidth: "250px", height: "330px", backgroundColor: getRandomColor() }}>
                        <div class="row g-0">
                            <div class="">
                                <div class="card-body">
                                    <h2 class="card-title">{item.plan_name}</h2>
                                    <p class="card-text fs-2 fw-bold"><small class="text-muted">Price :{item.price} $</small></p>
                                    <p class="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
                                    <button className='btn-sm btn-success' onClick={() => {
                                        navigate(`/payment/${item._id}`, {
                                            state: { array: item }
                                        })
                                    }}
                                    >Purchase</button>
                                </div>
                            </div>
                        </div>
                    </div>
                })}

            </div>
        </div>
    )
}

export default Membership


// onClick={()=>clickhandler(item)}  