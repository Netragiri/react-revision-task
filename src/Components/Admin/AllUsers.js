import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { Outlet } from 'react-router-dom'
import { getUsers, removeuser } from '../../Redux/User/UserActions'

function AllUsers() {
  const data = useSelector((state) => state.userlist)
  const { users, error, loading } = data
  // const [users,setUsers]=useState(data?.users)
  const dispatch = useDispatch()
  useEffect(() => {
    dispatch(getUsers())
    console.log(users)
  }, [])

  const deleteHandler = (id) => {
    if (window.confirm("confirm deletion")) {
      dispatch(removeuser(id));
    }
  };
  return (
    <div className='container'>

      <h1>Users</h1>
      <div className='text-end m-3 '>
        <button className='btn-primary p-2'>Add user</button>
      </div>

      <table className='table table-info'>
        <thead>
          <tr className='table-danger'>
            <th>Username</th>
            <th>Email-id</th>
            <th>Role</th>
            <th>Phone_no</th>
            <th>Status</th>
            <th>Actions</th>
          </tr>
        </thead>
        <tbody>
          {users?.map((item) => {
            return <tr>
              <td>{item.username}</td>
              <td>{item.email}</td>
              <td>{item.role}</td>
              <td>{item.phone_no}</td>
              <td>{item.status}</td>
              <td><button className='btn-danger m-2 p-1' onClick={() => deleteHandler(item._id)}>remove</button><button className='btn-success p-1'>update</button></td>
            </tr>
          })}
        </tbody>
      </table>
    </div>
  )
}

export default AllUsers
