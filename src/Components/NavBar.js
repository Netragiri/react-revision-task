import React from 'react'
import { Link } from 'react-router-dom'
import { toast } from 'react-toastify'
import { LogInAuth } from '../Utills/LogInAuth'


function NavBar() {
    // const role =  JSON.parse(localStorage.getItem("role"))
    // const [role,setRole]=useState(user)
    const auth = LogInAuth()
    const handlelogout = () => {
        // localStorage.removeItem("user")
        auth.logout();
        toast.success("logout successfully", { autoClose: 3000 })
    }
    // useEffect(()=>{
    //     setRole(user)
    // },[])
//   console.log(user)
    return (
        <div>
            < nav className="navbar navbar-expand-lg navbar-light bg-light">
                <Link className="navbar-brand" to="">Welcome</Link>
                <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                </button>
                <div className="collapse navbar-collapse" id="navbarNav">
                    <ul className="navbar-nav nav">
                        <li className="nav-item">
                            <Link className="nav-link" to="dashboard">Dashboard</Link>
                        </li>
                        <li className="nav-item">
                            <Link className="nav-link" to="Aboutus">About us</Link>
                        </li>
                        <li className="nav-item">
                            <Link className="nav-link" to="Contactus">Contact us</Link>
                        </li>
                    </ul>
                </div>
                {!auth.user && <><ul className='nav navbar-right p-2'>
                    <li className="nav-item">
                        <Link className="nav-link" to="Register">Register</Link>
                    </li>
                    <li className="nav-item">
                        <Link className="nav-link" to="login">Log In!!!!</Link>
                    </li>
                    
                </ul></>}
                {auth.user && <ul className='nav p-2'>
                    <li>
                        <Link className="nav-link" to="membership">Membership</Link>
                    </li>
                    <li>
                        <Link className="nav-link" to="add_news">Add news</Link>
                    </li>
                    <li>
                    <Link className="nav-link" to="alluser">users</Link>
                    </li>
                    
                    {/* {role == "admin" &&<>
                    <li>
                    <Link className="nav-link" to="">Add user</Link>
                </li>
                <li>
                <Link className="nav-link" to="">Add admin</Link>
            </li></>
                    
                       } */}
                    <li>
                        <Link className="nav-link" to="profile">Profile</Link>
                    </li>
                    <li className='nav-item'><Link to='/login'>  <button className='btn btn-primary' onClick={handlelogout}>Logout</button></Link></li></ul>}

            </nav>
        </div>
    )
}

export default NavBar
