import React from 'react'

function Pagination({newsperpage,totalnews,paginate}) {
    const pagenumbers=[];
    for(let i=1;i<=Math.ceil(totalnews/newsperpage);i++){
        pagenumbers.push(i)
    }
  return (
       
    <nav>
      <ul className='pagination'>
          {pagenumbers.map(number=>(
              <li key={number} className='page-item'>
                  <a onClick={(e)=>{paginate(number,e)}} href="" className='page-link'>
                      {number}
                  </a>
              </li>
          ))}
      </ul>
    </nav>
  )
}

export default Pagination
