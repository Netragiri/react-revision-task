import React, { useEffect } from 'react'
import { useSelector } from 'react-redux'

function Profile() {
    const user = JSON.parse(localStorage.getItem("user"))
    // const userlist=useSelector((state)=>state.userlist)
    // const {user}=userlist
    useEffect(() => {
        console.log(user)
    }, [])
    return (
        <div>
            <section class="vh-100" style={{ backgroundColor: '#f4f5f7' }}>
                <div class="container py-5 h-100">
                    <div class="row d-flex justify-content-center align-items-center h-100">
                        <div class="col col-lg-6 mb-4 mb-lg-0">
                            <div class="card mb-3">
                                <div class="row g-0">
                                    <div class="col-md-4 gradient-custom text-center text-white">
                                        <img src="https://mdbcdn.b-cdn.net/img/Photos/new-templates/bootstrap-chat/ava1-bg.webp"
                                            alt="Avatar" class="img-fluid my-5" style={{ width: "80px" }} />
                                        <h5>Marie Horwitz</h5>
                                        <p>Web Designer</p>
                                        <i class="far fa-edit mb-5"></i>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="card-body p-4">
                                            <h6>Information</h6>
                                            <hr class="mt-0 mb-4" />
                                            <div class="row pt-1">
                                                <div class="col-6 mb-3">
                                                    <h6>Email</h6>
                                                    <p class="text-muted">{user.email}</p>
                                                </div>
                                                <div class="col-6 mb-3">
                                                    <h6>Phone</h6>
                                                    <p class="text-muted">{user.phone_no}</p>
                                                </div>
                                            </div>
                        
                                            <hr class="mt-0 mb-4" />
                                            <div class="row pt-1">
                                                <div class=" mb-3">
                                                    <h6>Username:</h6>
                                                    <p class="text-muted">{user.username}</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>

    )
}

export default Profile
