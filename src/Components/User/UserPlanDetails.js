import axios from 'axios'
import React, { useEffect, useState } from 'react'
import Plan_News from '../News/Plan_News'

function UserPlanDetails() {
    const [userplan,setUserPlan]=useState({})
    useEffect(()=>{
     userplandetail()
    },[])
  
    const userplandetail=async()=>{
      const user = JSON.parse(localStorage.getItem("user"));
      // console.log(user._id)
    
      try{
          await axios.get(`http://localhost:4000/subscription/userplan/${user._id}`)
          .then(result=>setUserPlan(result.data) )
          .catch(err=>console.log("error",err.response.data.message))
  
      }
      catch(err){
        console.log(err.message)
      }
  }
  const plan_name=userplan?.plann_id?.plan_name
  return (
    <div>
      <h4>You are enjoying <strong>{plan_name} </strong> membership</h4>
      <Plan_News />
    </div>
  )
}

export default UserPlanDetails
