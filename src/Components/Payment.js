import React, { useEffect, useState } from 'react'
import { useDispatch } from 'react-redux'
import { useLocation, useNavigate } from 'react-router-dom'
import "../assets/CSS/payment.css"
import { buyplan } from '../Redux/membership/membershipAction'


function Payment() {
    const location = useLocation()
    const dispatch=useDispatch()
    const plannn = location?.state?.array
    const user = JSON.parse(localStorage.getItem("user"))
    const navigate =useNavigate()
    const [inputValues, setInputValue] = useState({
        cardNumber: "",
        expiry: "",
        cvv: ""
    });
    const [validation, setValidation] = useState({
        cardNumber: "",
        expiry: "",
        cvv: ""
    });

    function handleChange(event) {
        const { name, value } = event.target;
        setInputValue({ ...inputValues, [name]: value });
    }
    const checkvalidation = () => {
        let errors = validation;

        //first Name validation
        if (!inputValues.cardNumber) {
            errors.cardNumber = "Card number is required";
        } else {
            errors.cardNumber = "";
        }
        if (!inputValues.expiry) {
            errors.expiry = "Expirydate is required";
        } else {
            errors.expiry = "";
        }
        if (!inputValues.cvv) {
            errors.cvv = "cvv number is required";
        } else {
            errors.cvv = "";
        }
        setValidation(errors)
    }
    useEffect(() => {
        checkvalidation();
    }, [inputValues]);

    const handleSubmit = (e,item) => {
        e.preventDefault();
        console.log(plannn)
        dispatch(buyplan({ plannn, navigate }))
    }
        // alert("purchase successfully")
        // navigate('/dashboard')
        
    
    
       
    return (
        <div>
            <form onSubmit={handleSubmit}>
                <div class="container p-0">
                    <div class="card px-4">
                        <p class="h8 py-3">Payment Details</p>
                        <div class="row gx-3">
                            <div class="col-12">
                                <div class="d-flex flex-column">
                                    <p class="text mb-1">Person Name</p>
                                    <input class="form-control mb-3" type="text" placeholder="Name" value={user?.username} />
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="d-flex flex-column">
                                    <p class="text mb-1">Card Number</p>
                                    <input class="form-control mb-3" name='cardNumber' value={inputValues.cardNumber} type="text" placeholder="1234 5678 435678" onChange={(e) => handleChange(e)} required />
                                    <p>  {validation.cardNumber && <p>{validation.cardNumber}</p>}</p>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="d-flex flex-column">
                                    <p class="text mb-1">Expiry</p>
                                    <input class="form-control mb-3" name="expiry" value={inputValues.expiry} type="text" placeholder="MM/YYYY" onChange={(e) => handleChange(e)} required />
                                    <p>  {validation.expiry && <p>{validation.expiry}</p>}</p>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="d-flex flex-column">
                                    <p class="text mb-1">CVV/CVC</p>
                                    <input class="form-control mb-3 pt-2" name="cvv" value={inputValues.cvv} type="password" placeholder="***" onChange={(e) => handleChange(e)} required />
                                    <p>  {validation.cvv && <p>{validation.cvv}</p>}</p>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="d-flex flex-column">
                                    <p class="text mb-1">Plan name</p>
                                    <input class="form-control mb-3" type="text" value={plannn?.plan_name} onChange={(e) => e.target.value} />
                                </div>
                            </div>
                            <div class="col-12">
                                <button class="btn btn-primary mb-3" type='submit' >
                                    <span class="ps-3">Pay ${plannn?.price}</span>
                                    <span class="fas fa-arrow-right"></span>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    )
}

export default Payment
