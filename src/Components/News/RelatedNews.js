import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { useLocation, useNavigate } from 'react-router-dom'
import { fetchdata } from '../../Redux/News/Actions'

function RelatedNews() {
    const nav=useNavigate() 
    const dispatch=useDispatch()
    const [newss,setnews]=useState([])
    const data = useSelector((state) => state.newslist)
    const location=useLocation()
    const news=location.state.array

  //   useEffect(() => {
  //     dispatch(fetchdata())
      
      
  //     // console.log(users)
  //     // console.log(data)
  // }, [])
  // useEffect(()=>{
  // // setnews(data?.data?.articles)
  //     console.log(data?.data?.articles)
  // },[data])

  return (
    <div>
      <div className='row m-5'>
        <h4>Related news</h4>
        
        {data?.data?.articles.filter((item)=>
        (item.author===news.author && item.content !==news.content)
        ).map((item)=> {return <div class="card bg-secondary text-white text-center mt-4  col-md-5">
        <div class="card-body">
          <h5>{item.name}</h5>
          <h6 class="card-title">{item.title}</h6>
        </div>
        <button  class="btn btn-primary btn-sm"  onClick={() => {
                                                nav(`/detaildata/${item.id}`, {
                                                    state: { array: item }
                                                })
                                            }} >more info</button>
        <div class="card-footer text-muted">
          {item.publishedAt}
        </div>
      </div>}
        )}
      </div>
    </div>
  )
}

export default RelatedNews
