
import React from 'react'
import { useSelector } from 'react-redux'
import { useLocation, useNavigate } from 'react-router-dom'
import RelatedNews from './RelatedNews'


function DetailedData() {
  const nav=useNavigate()
  const data = useSelector((state) => state.datareducer)
  // console.log(data.data.articles)

  
    const location=useLocation()
    const news=location.state.array
  return (
    <div className='container'>
      <p className='fs-3'><strong>Author name :</strong>{news?.author!==null ? news?.author : "no author"}</p>
      <h3 className='m-3'>{news?.title}</h3>
      <img style={{height:'300px',width:'500px'}} src={news?.urlToImage}></img>
      <p className='m-3 fs-5'><strong>Publish date :</strong>{news?.publishdate}</p>
      <p className='fst-italic fs-5'><strong>Description : </strong>{news?.description}</p>
      <p className='fst-italic fs-5'><strong>Content : </strong>{news?.content}</p>
      <a className='fst-italic fs-5' href={news?.url}>tap to link for detail</a>

      <RelatedNews />
    </div>

  )
}

export default DetailedData
