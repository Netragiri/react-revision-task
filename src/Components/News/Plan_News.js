import axios from 'axios';
import React, { useEffect, useState } from 'react'
import { useNavigate } from 'react-router-dom';
import { detailhandler } from '../../Utills/CommanFunc';

function Plan_News() {
    const[news,setNews]=useState()
    const nav=useNavigate()
    useEffect(()=>{
        plan_news()
    },[])
    const plan_news=async()=>{
        const user = JSON.parse(localStorage.getItem("user"));
      
        try{
            await axios.get(`http://localhost:4000/news/?filter=${user.current_plan}`)
            .then(result=>setNews(result.data) )
            .catch(err=>console.log("error",err.response.data.message))
    
        }
        catch(err){
          console.log(err.message)
        }
    }
  return (
    <div className='container'>
        <div className='row'>
      {news?.map((item)=>(
          <div class="card border-secondaryk mb-3 mt-4 col-md-5" style={{maxWidth: "600px"}}>
          <div class="row g-0">
            <div class="col-md-4 mt-4">
              <img src={item.urlToImage} class="img-fluid rounded-start" alt="..." style={{height:"150px"}} />
            </div>
            <div class="col-md-8">
              <div class="card-body"style={{height: "200px"}} >
                <h5 class="card-title fw-bold">{item.name}</h5>
                <p class="card-text fst-italic">{item.title.slice(0,60).concat("...")}</p>
                <button className='btn btn-secondary btn-sm mt-3' onClick={() =>detailhandler(item,nav)}>more details</button>
                                            <button className='btn btn-outline-secondary btn-sm ms-3 mt-3' onClick={() => {
                                                nav(`/update_news/${item.id}`, {
                                                    state: { array: item }
                                                })
                                            }}>update news</button>
              </div>
                <p class="card-text mb-2"><small class="text-muted">{item.publishedAt}</small></p>
            </div>
          </div>
        </div>
      ))}
      </div>
    </div>
  )
}

export default Plan_News
