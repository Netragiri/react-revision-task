import { Field, Form, Formik } from 'formik'
import React from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { addnews } from '../../Redux/News/Actions'

function Add_news() {
  const data = useSelector((state) => state.datareducer)
  const { loading } = data
  const dispatch = useDispatch()


  const handlesubmit = (data) => {
    console.log(data)
    dispatch(addnews(data))
  }
  return (
    <div>
      <Formik
        //  validationSchema={validation}
        initialValues={{
          id: '',
          username: '',
          author: '',
          title: '',
          description: '',
          content: '',
          urlToImage: '',
          url: ''
        }}
        onSubmit={handlesubmit}>


        <div className='container my-5'>
          <Form>

            <strong>Id:</strong>
            <Field type='number' name='id' placeholder='enter id' className='form-control' />

            <strong>Name:</strong>
            <Field type='text' name='name' placeholder='enter your name' className='form-control' />

            <strong>Author:</strong>
            <Field type='text' name='author' placeholder="enter author name" className='form-control' />

            <strong>Title:</strong>
            <Field type='text' name='title' placeholder="enter news title" className='form-control' />

            <strong>Description:</strong>
            <Field type='text' name='description' placeholder="description" className='form-control' />

            <strong>Content:</strong>
            <Field type='text' name='content' placeholder="content" className='form-control' />

            <strong>urlToImage:</strong>
            <Field type='url' name='urlToImage' placeholder="enter url" className='form-control' />

            <strong>url:</strong>
            <Field type='url' name='url' placeholder="enter url" className='form-control' />


            <button type='submit' className='my-5 btn-secondary'>Add news</button>


          </Form>
        </div>
      </Formik>
    </div>
  )
}

export default Add_news
