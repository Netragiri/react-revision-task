import { Field, Form, Formik } from 'formik'
import React, { useEffect } from 'react'
import { useDispatch } from 'react-redux'
import { useLocation } from 'react-router-dom'
import { updateNews } from '../../Redux/News/Actions'

function Update_news() {
    const dispatch=useDispatch()
    const location=useLocation()
    const handlesubmit=(data)=>{
        console.log(data)
        dispatch(updateNews({
            formdata:{...data},
            id:data.id
        }))
    }
    // useEffect(()=>{
    //     console.log(typeof location.state.array.name)
    // },[])
    const news=location.state.array
    return (
        <div>
            <Formik
                //  validationSchema={validation}
                initialValues={{
                    id:news.id,
                    name: news.name,
                    author: news.author,
                    title: news.title,
                    description: news.description,
                    content: news.content,
                    urlToImage: news.urlToImage,
                    url: news.url
                }}
                onSubmit={handlesubmit}>


                <div className='container my-5'>
                    <Form>

                    <strong>Id:</strong>
                        <Field type='text' name='id' placeholder='enter id' className='form-control' />

                        <strong>Name:</strong>
                        <Field type='text' name='name' placeholder='enter your name' className='form-control' />

                        <strong>Author:</strong>
                        <Field type='text' name='author' placeholder="enter author name" className='form-control' />

                        <strong>Title:</strong>
                        <Field type='text' name='title' placeholder="enter news title" className='form-control' />

                        <strong>Description:</strong>
                        <Field type='text' name='description' placeholder="description" className='form-control' />

                        <strong>Content:</strong>
                        <Field type='text' name='content' placeholder="content" className='form-control' />

                        <strong>urlToImage:</strong>
                        <Field type='url' name='urlToImage' placeholder="enter url" className='form-control' />

                        <strong>url:</strong>
                        <Field type='url' name='url' placeholder="enter url" className='form-control' />


                        <button type='submit' className='my-5 btn-secondary'>Update news</button>


                    </Form>
                </div>
            </Formik>
        </div>
    )
}

export default Update_news
