import logo from './logo.svg';
import './App.css';
import { Route, Routes } from 'react-router-dom';
import Home from './Screens/Home';
import Register from './Protected screen/Register';
import Login from './Protected screen/Login';
import Protected from './Utills/Protected';
import Dashboard from './Components/Dashboard';
import { AuthProvider } from './Utills/LogInAuth';
import NavBar from './Components/NavBar';
import { Provider } from 'react-redux';
import store from './Redux/Store';
import DetailedData from './Components/News/DetailedData';
import Footer from './Components/Footer';
import About_us from './Components/About_us';
import Contact_us from './Components/Contact_us';
import Add_news from './Components/News/Add_news';
import Update_news from './Components/News/Update_news';
import Membership from './Components/Membership ';
import Profile from './Components/User/Profile';
import Payment from './Components/Payment';
import AdminDashboard from './Components/Admin/AdminDashboard';
import Users from './Components/Admin/AllUsers';
import AllUsers from './Components/Admin/AllUsers';

function App() {
  return (
    <div className="App">
      <Provider store={store}>
        <AuthProvider>
          <NavBar />
          <Routes>
         
            <Route path='register' element={<Register />}></Route>
            <Route path='alluser' element={<Protected><AllUsers /></Protected>}></Route>
            <Route path='' element={<Home />}></Route>
            <Route path='login' element={<Login />}></Route>
            <Route path='aboutus' element={<About_us />}></Route>
            <Route path='contactus' element={<Contact_us />}></Route>
            <Route path='contactus' element={<Contact_us />}></Route>
            <Route path='membership' element={<Protected><Membership /></Protected>} />
            <Route path='Profile' element={<Protected> <Profile /></Protected>} />

            <Route path='update_news/:id' element={<Protected><Update_news /></Protected>} />

            <Route path='dashboard' element={ <Dashboard />} />

            <Route path='/detaildata/:id' element={<DetailedData />}></Route>
            <Route path='payment/:id' element={<Protected><Payment /></Protected>}></Route>


            {/* adminRoutes */}
            <Route path="admindashboard" element={<Protected><AdminDashboard /></Protected>}></Route>
           
          </Routes>
          <Footer />
        </AuthProvider>
      </Provider>


    </div>
  );
}

export default App;
