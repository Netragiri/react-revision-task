import React, { useEffect } from 'react'
import UserForm from './UserForm';


function Register() {

  return (
    <div>
      <h1>Sign up</h1>
      <UserForm />
    </div>
  )
}

export default Register
