import { ErrorMessage, Field, Form, Formik } from 'formik'
import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { useNavigate } from 'react-router-dom'
import { register } from '../Redux/User/UserActions'
import { initialValues, validation } from '../Utills/Validations'

function UserForm({shouldNavigate=true}) {
    const navigate=useNavigate()
    const dispatch=useDispatch()
    // const user=useSelector((state)=>state.userlist)


    //to print error message for validation
  const Error = ({ name }) => {
    return (
      <>
        <div style={{ color: 'red' }}>
          <ErrorMessage name={name} />
        </div>
      </>
    )
  }
  return (
    <div>
       <Formik validationSchema={validation} initialValues={initialValues}
        onSubmit={(item) => {
          dispatch(register({
            formdata:{...item},
            navigate,
            shouldNavigate
          }))
          // localStorage.setItem("data", btoa(JSON.stringify(item)))
        //   navigate('/login')
          console.log(item)
        }}>

        <div className='container' >

          <Form>

            <strong>Username:</strong>
            <Field type='text' placeholder='enter your name here' className='form-control' name='username' id='username' /><Error name='username' />

            <strong>Email-id:</strong>
            <Field type='text' placeholder='email-id' id='email' className='form-control' name='email' /><Error name='email' />

            <strong> Password:</strong>
            <Field type='password' placeholder='enter password' id='password' className='form-control' name='password' /><Error name='password' />

            <strong>Confirm_password:</strong>
            <Field type='password' placeholder='confirm password' id='confirm_password' className='form-control' name='confirm_password' /><Error name='confirm_password' />

            <strong> Phone_no:</strong>
            <Field type='number' placeholder='enter phone no.' id='number' className='form-control' name='number' /><Error name='number' />


            <strong>City:</strong>
            <Field as='select' className="form-control form-control-sm" name='city'>
              <option value=''>select</option>
              <option value='gujrat'>Gujrat</option>
              <option value='RAJASTHAN'>Rajsthan</option>
              <option value='keral'>Keral</option>
              <option value='others'>Others</option>
            </Field><Error name='city' />

        
            <button type='submit' className='my-5'>Submit</button>

          </Form>
        </div>

      </Formik>
    </div>
  )
}

export default UserForm
