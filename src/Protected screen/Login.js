import { Formik, Form, Field, ErrorMessage } from 'formik'
import React, { useCallback, useEffect, useMemo, useState } from 'react'
import { useDispatch } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import { Link } from 'react-router-dom'
import { login, userprofile } from '../Redux/User/UserActions';
import { LogInAuth } from '../Utills/LogInAuth';
import * as yup from 'yup';



//first get data from the localstorage 

function Login() {
  const auth = LogInAuth()
  const dispatch = useDispatch()
  const navigate = useNavigate()
  const [role,setRole]=useState()
  useEffect(() => {
    dispatch(userprofile())
  }, [])

  //for to check the correct autorise user then navigate the user to the dashboard
  const handlesubmit = (values) => {
    auth.login(values.username)
    if(role=="user"){
      dispatch(login({
        formdata: { ...values },
        navigate,
        role
      }))
    // localStorage.setItem()
    }
    else if(role=="admin"){
      dispatch(login({
        formdata: { ...values },
        navigate,
        role
      }))
    }
    else{
      alert("select role")
    }
    console.log(values)
  }


  const validation=yup.object().shape({
    username:yup.string().required("required"),
    password:yup.string().required("required"),
  })
  const Error = ({ name }) => {
    return (
      <>
        <div style={{ color: 'red' }}>
          <ErrorMessage name={name} />
        </div>
      </>
    )
  }
  return (
    <div>
      <h1>Log In</h1>
      <Formik
         validationSchema={validation}
        initialValues={{
          username: '',
          password: '',
        }}
        onSubmit={handlesubmit}>


        <div className='container my-5'>
          <Form>
            <strong>Username:</strong>
            <Field type='text' name='username' placeholder='enter your name' className='form-control' />
            <Error name='username' />


            <strong>Password:</strong>
            <Field type='password' name='password' placeholder='enter your password' className='form-control' />
            <Error name='password' />

            <select name="role" as="select" className="m-3"  onChange={(e) =>setRole(e.target.value)}>
              <option value="" defaultValue='selected' >select role</option>
              <option value="admin">Admin</option>
              <option value="user">user</option>
            </select>
                    
                    <button type='submit' className='my-5 btn-secondary'>Log in</button>

            <p>Don't have an account</p>
            <Link to='/register'>Sign Up</Link>
          </Form>
        </div>
      </Formik>
    </div>
  )
}

export default Login
