import axios from "axios";
import { BUY_PLAN_FAIL, BUY_PLAN_REQ, BUY_PLAN_SUCCESS, FETCH_MEMBERSHIP_ERROR, FETCH_MEMBERSHIP_REQ, FETCH_MEMBERSHIP_SUCCESS, SINGLE_USERPLAN_FAIL, SINGLE_USERPLAN_REQ, SINGLE_USERPLAN_SUCCESS } from "./membershipTypes";

const fetchDataRequest = () => {
    return {
        type: FETCH_MEMBERSHIP_REQ,
        loading: true
    }
}

const fetchDataSuccess = (data) => {
    return {
        type: FETCH_MEMBERSHIP_SUCCESS,
        payload: data,
    }
}

const fetchDataError = (error) => {
    return {
        type: FETCH_MEMBERSHIP_ERROR,
        payload: error,
    }
}


export const getmemberships =
    () => async (dispatch) => {
        dispatch(fetchDataRequest())
        await axios.get('http://localhost:4000/membership/getplans')
            //    https://saurav.tech/NewsAPI/top-headlines/category/health/in.json
            .then(res => {
                // console.log("api called")
                dispatch(fetchDataSuccess(res.data))
            })
            .catch(err => dispatch(fetchDataError(err.message)))

    }

export const buyplan = ({plannn,navigate}) =>
async (dispatch) => {
        const user = JSON.parse(localStorage.getItem("user"));
        user.current_plan=plannn._id;
        localStorage.setItem('user',JSON.stringify(user));
        const plan = {
            user_id: user._id,
            plann_id:plannn._id
        }
        dispatch({
            type:BUY_PLAN_REQ
        });
        
        try{
            const config = {
                headers: {
                    "Content-Type": "application/json",
                },
            };
            console.log(plan)
            const res=await axios.post('http://localhost:4000/subscription/buyplan',plan,config)
            .then(res=>alert("purchase successfully"))
            // .then(res=>{return alert("purchase successfully"),navigate("/dashboard")})
            .catch(err=>alert(err.response.data.message))
            console.log(res.data)
            dispatch({
                type:BUY_PLAN_SUCCESS,
            })
        }
        catch(err){
            // console.log("hi")
            dispatch({
                type: BUY_PLAN_FAIL,
                payload: err.response ? err.response.data : err.message,
              });
    
        }
       
    }

    export const userplan=()=>async(dispatch)=>{
        const user = JSON.parse(localStorage.getItem("user"));
        console.log(user._id)
        dispatch({
            type:SINGLE_USERPLAN_REQ
        })

        try{
            await axios.get(`http://localhost:4000/subscription/userplan/${user._id}`)
            .then(result=>alert(result.data.plann_id.plan_name))
            .catch(err=>console.log("error",err.response.data.message))
            dispatch({
                type:SINGLE_USERPLAN_SUCCESS,
            })

        }
        catch(err){
            // console.log("hi")
            dispatch({
                type: SINGLE_USERPLAN_FAIL,
                payload: err.response ? err.response.data : err.message,
              });
    
        }
    }