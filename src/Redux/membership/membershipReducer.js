import { FETCH_MEMBERSHIP_ERROR, FETCH_MEMBERSHIP_REQ, FETCH_MEMBERSHIP_SUCCESS } from "./membershipTypes"

const initialstate={
    data:[],
    error:"",
    loading:false
}


const membershipreducer=(state=initialstate,action)=>{
    switch(action.type){
        case FETCH_MEMBERSHIP_REQ:
            return{
                ...state,
                loading:true,
            }
        case FETCH_MEMBERSHIP_ERROR:
            return{
                data:[],
                error:action.payload,
                loading:false,
            }
        case FETCH_MEMBERSHIP_SUCCESS:
                return{
                    ...state,
                    data:action.payload,
                    loading:false,
                }     
        default :return state
    }
}

export default membershipreducer