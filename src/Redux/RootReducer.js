import membershipreducer from "./membership/membershipReducer";
import datareducer from "./News/Reducer";
import  userReducer  from "./User/UserReducer";

const { combineReducers } = require("redux");


const rootreducer=combineReducers({
    newslist:datareducer,
    userlist:userReducer,
    membershiplist:membershipreducer
})
export default rootreducer