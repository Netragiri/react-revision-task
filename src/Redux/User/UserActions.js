import axios from "axios";
import { ADD_USER_FAIL, ADD_USER_REQUEST, ADD_USER_SUCCESS, GET_ALL_USERS_FAIL, GET_ALL_USERS_REQUEST, GET_ALL_USERS_SUCCESS, LOGIN_USER_FAIL, LOGIN_USER_REQUEST, LOGIN_USER_SUCCESS, REMOVE_USER_FAIL, REMOVE_USER_REQUEST, REMOVE_USER_SUCCESS, USER_PROFILE_FAIL, USER_PROFILE_REQUEST, USER_PROFILE_SUCCESS } from "./UserTypes";
import bcrypt from "bcryptjs"


//register user
export const register =
  ({ formdata, navigate }) => async (dispatch) => {
    const data = {
      username: formdata.username,
      password: formdata.password,
      email: formdata.email,
      phone_no: formdata.number,
    };

    dispatch({
      type: ADD_USER_REQUEST
    })

    try {
      const config = {
        headers: {
          "Content-Type": "application/json",
        },
      };
      const res = await axios.post("http://localhost:4000/user/register", data, config)
        .then(() => navigate("/login"))
        .catch(err => alert(err.response.data.message))         // console.log({ ...data, id: res.data.id });

      dispatch({
        type: ADD_USER_SUCCESS,
        payload: { ...data, id: res.data.id },
      });

    }
    catch (err) {
      dispatch({
        type: ADD_USER_FAIL,
        payload: err.response ? err.response.data : err.message,
      });
    }
  }

//login user
export const login = 
  ({ formdata, navigate,role }) => async (dispatch,getState) => {
    const data={
      username:formdata.username,
      password:formdata.password,
      role:role
    };
    dispatch({
      type:LOGIN_USER_REQUEST
    })

    try {
      const config = {
        headers: {
          "Content-Type": "application/json",
        },
      };
      const res = await axios.post("http://localhost:4000/user/login", data, config)
        .then((res) => (
          dispatch({
            type: LOGIN_USER_SUCCESS,
            payload: { user:res.data.data},
          }),
          navigate("/dashboard"),
          localStorage.setItem("user",JSON.stringify(res.data.data)),
          localStorage.setItem("token",res.data.token),
          localStorage.setItem("role",res.data.data.role)
        ))
        .catch(err => alert(err.response.data.message))       
     
    }
    catch (err) {
      dispatch({
        type: LOGIN_USER_FAIL,
        payload: err.response ? err.response.data : err.message,
      });
    }
}


//get all users
// Get All Users
export const getUsers = () => async (dispatch) => {
  dispatch({
    type: GET_ALL_USERS_REQUEST,
  });

  try {
    const token=localStorage.getItem("token")
    const res = await axios.get("http://localhost:4000/user/allusers",{
      headers:{"Authorization": `Bearer ${token}` }
    }).then(res=> dispatch({
      type: GET_ALL_USERS_SUCCESS,
      payload: res.data,
    })).catch(err=>alert(err.response.data.message));
   
  } catch (err) {
    dispatch({
      type: GET_ALL_USERS_FAIL,
      payload: err.response ? err.response.data : err.message,
    });
  }
};




//user profile
export const userprofile = (id) => async (dispatch) => {
  dispatch({
    type: USER_PROFILE_REQUEST,
  });

  try {
    const res = await axios.get(`http://localhost:4000/user/profile/${id}`);
    dispatch({
      type: USER_PROFILE_SUCCESS,
      payload: res.data,
    });
  } catch (err) {
    dispatch({
      type: USER_PROFILE_FAIL,
      payload: err.response ? err.response.data : err.message,
    });
  }
};


export const removeuser=(id)=>async(dispatch)=>{
  dispatch({
    type:REMOVE_USER_REQUEST
  })
  try{
    const res = await axios.patch(`http://localhost:4000/user/removeuser/${id}`)
    .then(res=>{
      dispatch({
        type:REMOVE_USER_SUCCESS,
        payload:id
      })
    })
    .catch(err=>console.log(err))
  }
  catch(err){
    dispatch({
      type:REMOVE_USER_FAIL,
      payload: err.response ? err.response.data : err.message,
    })
  }
}