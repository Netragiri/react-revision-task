import { ADD_NEWS_FAIL, ADD_NEWS_SUCCESS } from "../News/Action-Constants"
import { ADD_USER_FAIL, ADD_USER_REQUEST, ADD_USER_SUCCESS, GET_ALL_USERS_FAIL, GET_ALL_USERS_REQUEST, GET_ALL_USERS_SUCCESS, LOGIN_USER_REQUEST, LOGIN_USER_SUCCESS, REMOVE_USER_FAIL, REMOVE_USER_REQUEST, REMOVE_USER_SUCCESS, USER_PROFILE_FAIL, USER_PROFILE_REQUEST, USER_PROFILE_SUCCESS } from "./UserTypes"


const initialState = {
    users: [],
    error: "",
    loading: false,
    user: null
}

let user = localStorage.getItem("user");
const userReducer = (state = initialState, action) => {
    // console.log("state",state,action)
    switch (action.type) {
        case ADD_USER_REQUEST:
        case GET_ALL_USERS_REQUEST:
        case USER_PROFILE_REQUEST:
        case LOGIN_USER_REQUEST:
        case REMOVE_USER_REQUEST:
            return {
                ...state,
                loading: true
            }
        case GET_ALL_USERS_SUCCESS:
            return {
                ...state,
                users: action.payload,
                loading: false,
            };
        case ADD_USER_FAIL:
        case USER_PROFILE_FAIL:
        case GET_ALL_USERS_FAIL:
        case REMOVE_USER_FAIL:
            return {
                ...state,
                error: action.payload,
                loading: false
            }
        case ADD_USER_SUCCESS:
            return {
                ...state,
                users: [...state.users, action.payload],
                loading: false,
            }
        case LOGIN_USER_SUCCESS:
            // localStorage.setItem("user", JSON.stringify(action.payload.user));
            return {
                ...state,
                user: action.payload.user,
                loading: false,
            }
        case USER_PROFILE_SUCCESS:
            return {
                ...state,
                user: action.payload,
                loading: false
            }
        case REMOVE_USER_SUCCESS:
            // console.log("reducers",action.payload)
            console.log(state)
            const data=state.users.filter((deletedUser)=>deletedUser._id!==action.payload)
            return{
                ...state,
                users:data,
                loading:false
            }
        default: return state
    }
}

export default userReducer