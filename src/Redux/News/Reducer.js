import { ADD_NEWS_FAIL, ADD_NEWS_REQ, ADD_NEWS_SUCCESS, FETCH_DATA_ERROR, FETCH_DATA_REQ, FETCH_DATA_SUCCESS, UPDATE_NEWS_FAIL, UPDATE_NEWS_REQ, UPDATE_NEWS_SUCCESS } from "./Action-Constants"

const initialstate={
    data:[],
    error:"",
    loading:false
}

const datareducer=(state=initialstate,action)=>{
    switch(action.type){
        case FETCH_DATA_REQ:
        case ADD_NEWS_REQ:
        case UPDATE_NEWS_REQ:
            return{
                ...state,
                loading:true,
            }
        case FETCH_DATA_ERROR:
        case ADD_NEWS_FAIL:
        case UPDATE_NEWS_FAIL:
            return{
                data:[],
                error:action.payload,
                loading:false,
            }
        case FETCH_DATA_SUCCESS:
                return{
                    ...state,
                    data:action.payload,
                    loading:false,
                }
        case ADD_NEWS_SUCCESS:
            return{
                ...state,
                data:[...state.data,action.payload],
                loading:false
            }
        case UPDATE_NEWS_SUCCESS:
                return{
                    ...state,
                    data:state.data.map((news)=>
                        news.id==action.payload.id ? action.payload : news
                    ),
                    loading:false
                }
            
        default :return state
    }
}

export default datareducer