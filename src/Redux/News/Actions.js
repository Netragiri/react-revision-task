import axios from "axios"
import { ADD_NEWS_FAIL, ADD_NEWS_REQ, ADD_NEWS_SUCCESS, FETCH_DATA_ERROR, FETCH_DATA_REQ, FETCH_DATA_SUCCESS, UPDATE_NEWS_FAIL, UPDATE_NEWS_REQ, UPDATE_NEWS_SUCCESS } from "./Action-Constants"

const fetchDataRequest=()=>{
    return {
        type:FETCH_DATA_REQ,
        loading:true
    }
}

const fetchDataSuccess=(data)=>{
    return{
        type:FETCH_DATA_SUCCESS,
        payload:data,
    }
}

const fetchDataError=(error)=>{
    return{
        type:FETCH_DATA_ERROR,
        payload:error,
    }
}

//thunk function for fetch the data
function fetchdata(){
    return async function (dispatch){
        dispatch(fetchDataRequest())
       await axios.get('http://localhost:4000/news/getnews')
    //    https://saurav.tech/NewsAPI/top-headlines/category/health/in.json
        .then(res=>{
            // console.log("api called")
            dispatch(fetchDataSuccess(res.data))  
        })
        .catch(err=>dispatch(fetchDataError(err.message))) 
    }
}
export {fetchDataError,fetchDataSuccess,fetchDataRequest,fetchdata}


//addnews
export const addnews=(formdata)=>async(dispatch)=>{
    console.log("called")
    const data={
            name: formdata.name,
            id:formdata.id,
            author: formdata.author,
            title: formdata.title,
            description: formdata.description,
            urlToImage: formdata.urlToImage,
            url: formdata.url,
            content: formdata.content,
        }

        dispatch({type:ADD_NEWS_REQ})

            const config = {
                headers: {
                  "Content-Type": "application/json",
                },
              };
              const res = await axios.post('http://localhost:4000/news/createnews', data, config)
                .then(res=>alert(res.data.message))
                .catch(error=>alert( error.response.data.message))
            //   console.log(res.data);
            // console.log(res.data)
            // console.log(data)
              dispatch({
                type: ADD_NEWS_SUCCESS,
                payload: { ...data, data: res.data },
              });
    }

//update news
export const updateNews=({id,formdata})=>async(dispatch)=>{
    const data={
        name: formdata.name,
        id:formdata.id,
        author: formdata.author,
        title: formdata.title,
        description: formdata.description,
        urlToImage: formdata.urlToImage,
        url: formdata.url,
        content: formdata.content,
    }
    dispatch({
        type:UPDATE_NEWS_REQ
    });

    try{
        const config = {
            headers: {
              "Content-Type": "application/json",
            },
          };
          console.log(id)
          const res=await axios.patch(`http://localhost:4000/news/updatenews/${id}`,data,config)
          console.log(res.data)
          dispatch({
              type:UPDATE_NEWS_SUCCESS,
              payload:{...data,id:res.data.id}
          })
    }
    catch(err){
        dispatch({
            type: UPDATE_NEWS_FAIL,
            payload: err.response ? err.response.data : err.message,
          });

    }
}
