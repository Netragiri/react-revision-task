import { useEffect, useState } from "react"

function getsavedvalue(initialvalue){
const savedvalue=JSON.parse(localStorage.getItem(key))
if(savedvalue) return savedvalue 

return initialvalue
}
function useLocalStorage(initialvalue) {
    const[value,setValue]=useState(()=>{
      return getsavedvalue(key,initialvalue)
    })
useEffect(()=>{
  localStorage.setItem(JSON.stringify(value))
},[value])
    
  return [value,setValue]
}

export default useLocalStorage
