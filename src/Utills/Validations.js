import * as yup from 'yup';
const validation=yup.object().shape({
    username:yup.
              string().
              min(5).
              required(),
    number:yup.
            string().
            matches(/^[0-9]{10}$/,'must be a number and length should be 10').
            required(),
    email:yup.
            string().
            email().
            required(),
    password:yup.
              string().
              matches(/^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{8,}$/,'At least contain one letter one number and one special character').
              min(8,'password is too short').
              max(20). 
              required(),
    confirm_password:yup.
              string().
              oneOf([yup.ref('password')], 'Your passwords do not match.').
              required(),
    city:yup.
        string().
        required('*required'),    
  })

  const initialValues={
    username:'',
    email:'',
    password:'',
    confirm_password:'',
    number:'',
    city:'',
}
  
 


export  {validation,initialValues}